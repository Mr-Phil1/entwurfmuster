package eu.mrPhil;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CredentialsJSONExporter implements CredentialsExporter {
    @Override
    public void export(List<Credentials> credentialsList) {


        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("export.json"));
            String exportString = "[\n";
            for (int i = 0; i < credentialsList.size(); i++) {
                Credentials credentials = credentialsList.get(i);
                exportString += "{\"host\":" + "\"" + credentials.getHost() + "\"" + "," + "\"pwd\":" + "\"" + credentials.getUser() + "\"" + "," + "\"user\":" + "\"" + credentials.getPwd() + "\"" + "}";
                if (i < credentialsList.size() - 1) {
                    exportString += ",\n";
                }
            }
            exportString += "\n]";
            writer.write(exportString);
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}

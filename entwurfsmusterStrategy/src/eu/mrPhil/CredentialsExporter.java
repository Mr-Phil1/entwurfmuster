package eu.mrPhil;

import java.util.List;

public interface CredentialsExporter {
    void export(List<Credentials> credentialsList);
}

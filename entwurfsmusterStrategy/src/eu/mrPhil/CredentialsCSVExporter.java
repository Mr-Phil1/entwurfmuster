package eu.mrPhil;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CredentialsCSVExporter implements CredentialsExporter {
    @Override
    public void export(List<Credentials> credentialsList) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("export.csv"));
            String exportString = "HOST;USERNAME;PASSWORD\n";
            for (Credentials credentials : credentialsList) {
                exportString += credentials.getHost() + ";" + credentials.getUser() + ";" + credentials.getPwd() + "\n";
            }
            writer.write(exportString);
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}

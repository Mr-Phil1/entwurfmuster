package eu.mrPhil;

import java.util.List;
import java.util.Objects;

public class CredentialsExporterKontext {
    private CredentialsExporter credentialsExporter;

    public CredentialsExporterKontext(CredentialsExporter credentialsExporter) {
        Objects.requireNonNull(credentialsExporter);
        this.setCredentialsExporter(credentialsExporter);
    }

    public void setCredentialsExporter(CredentialsExporter credentialsExporter) {
        this.credentialsExporter = credentialsExporter;
    }

    public void exportAllCredentials(List<Credentials> credentialsList) {
        this.credentialsExporter.export(credentialsList);
    }
}

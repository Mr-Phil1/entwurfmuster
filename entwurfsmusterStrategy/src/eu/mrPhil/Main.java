package eu.mrPhil;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Credentials> credentialsList = new ArrayList<>();
        credentialsList.add(new Credentials("www.gmx.at", "123456789", "a.iller"));
        credentialsList.add(new Credentials("www.hotmail.com", "bhvghlvljh", "corban.nerum"));
        credentialsList.add(new Credentials("www.xyz.net", "5485f8e4sd", "bobba.fett"));

        CredentialsExporterKontext exporter = new CredentialsExporterKontext(new CredentialsJSONExporter());
        exporter.exportAllCredentials(credentialsList);
        exporter.setCredentialsExporter(new CredentialsCSVExporter());
        exporter.exportAllCredentials(credentialsList);


        //SOLID-Prinzip
        //Single Responsibility
        //Open-Closed-Principle
        //Liskov Substitution
        //Interface Segregation
        //Dependency Inversion
    }
}

package eu.mrPhil.chainOfResponsibility;

import eu.mrPhil.Credentials;

import java.util.List;

public class ExportBearbeiterCSV extends ExportBearbeiter {
    public ExportBearbeiterCSV(ExportBearbeiter naechsterExportBearbeiter) {
        super(naechsterExportBearbeiter);
    }

    @Override
    public void exportieren(String format, List<Credentials> credentialsList) {
        if (format.equals("CSV")) {
            String exportString = "";
            for (int i = 0; i < credentialsList.size(); i++) {
                Credentials credentials = credentialsList.get(i);
                exportString += credentials.getHost() + ";" + credentials.getUser() + ";" + credentials.getPwd();
                if (i < credentialsList.size() - 1) {
                    exportString += "\n";
                }
            }

            System.out.println(exportString);
        } else {
            super.exportieren(format, credentialsList);
        }
    }
}

package eu.mrPhil.chainOfResponsibility;

import eu.mrPhil.Credentials;

import java.util.List;

public abstract class ExportBearbeiter {

    private ExportBearbeiter naechsterExportBearbeiter;

    public ExportBearbeiter(ExportBearbeiter naechsterExportBearbeiter) {
        this.naechsterExportBearbeiter = naechsterExportBearbeiter;
    }

    public void exportieren(String format, List<Credentials> credentialsList) {
        if (this.naechsterExportBearbeiter != null) {
            naechsterExportBearbeiter.exportieren(format, credentialsList);
        } else {
            System.out.println("Keine Zuständigkeit gefunden");
        }
    }
}

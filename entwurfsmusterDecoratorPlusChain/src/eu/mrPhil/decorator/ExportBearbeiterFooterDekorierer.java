package eu.mrPhil.decorator;

import eu.mrPhil.Credentials;
import eu.mrPhil.chainOfResponsibility.ExportBearbeiter;

import java.util.List;

public class ExportBearbeiterFooterDekorierer extends ExportBearbeiterDekorierer {
    public ExportBearbeiterFooterDekorierer(ExportBearbeiter naechsterExportBearbeiter) {
        super(naechsterExportBearbeiter);
    }

    @Override
    public void exportieren(String format, List<Credentials> credentialsList) {
        super.exportieren(format, credentialsList);
        System.out.println("FOOTER DEKORATION");
    }
}

package eu.mrPhil.decorator;

import eu.mrPhil.Credentials;
import eu.mrPhil.chainOfResponsibility.ExportBearbeiter;

import java.util.List;

public abstract class ExportBearbeiterDekorierer extends ExportBearbeiter{

    public ExportBearbeiterDekorierer(ExportBearbeiter naechsterExportBearbeiter) {
        super(naechsterExportBearbeiter);
    }

    @Override
    public void exportieren(String format, List<Credentials> credentialsList) {
        super.exportieren(format, credentialsList);
    }
}

package eu.mrPhil.decorator;

import eu.mrPhil.Credentials;
import eu.mrPhil.chainOfResponsibility.ExportBearbeiter;

import java.util.List;

public class ExportBearbeiterHeaderDekorierer extends ExportBearbeiterDekorierer {
    public ExportBearbeiterHeaderDekorierer(ExportBearbeiter naechsterExportBearbeiter) {
        super(naechsterExportBearbeiter);
    }

    @Override
    public void exportieren(String format, List<Credentials> credentialsList) {
        System.out.println("HEADER DEKORATION");
        super.exportieren(format, credentialsList);
    }
}

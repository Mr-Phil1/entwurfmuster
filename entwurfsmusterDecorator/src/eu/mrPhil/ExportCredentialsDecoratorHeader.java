package eu.mrPhil;

import java.util.List;

public class ExportCredentialsDecoratorHeader extends ExportCredentialsDecorator {
    public ExportCredentialsDecoratorHeader(ExportCredentials exportCredentials) {
        super(exportCredentials);
    }

    public void export(List<Credentials> credentialsList) {
        System.out.println("HEADER FÜR EXPORT");
        super.export(credentialsList);
    }
}

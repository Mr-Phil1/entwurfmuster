package eu.mrPhil;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Credentials> credentialsList = new ArrayList<>();
        credentialsList.add(new Credentials("www.gmx.at", "123456789", "a.iller"));
        credentialsList.add(new Credentials("www.hotmail.com", "bhvghlvljh", "corban.nerum"));
        credentialsList.add(new Credentials("www.xyz.net", "5485f8e4sd", "bobba.fett"));

        ExportCredentials exportCredentials = new ExportCredentialsDecoratorHeader(new ExportCredentialsDecoratorFooter(new CsvExporter()));
        exportCredentials.export(credentialsList);
    }
}

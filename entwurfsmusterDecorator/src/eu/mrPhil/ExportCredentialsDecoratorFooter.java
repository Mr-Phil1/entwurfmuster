package eu.mrPhil;

import java.util.List;

public class ExportCredentialsDecoratorFooter extends ExportCredentialsDecorator {
    public ExportCredentialsDecoratorFooter(ExportCredentials exportCredentials) {
        super(exportCredentials);
    }

    public void export(List<Credentials> credentialsList) {
        super.export(credentialsList);
        System.out.println("FOOTER FÜR EXPORT");
    }
}

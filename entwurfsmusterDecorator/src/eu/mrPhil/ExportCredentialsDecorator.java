package eu.mrPhil;

import java.util.List;

public abstract class ExportCredentialsDecorator implements ExportCredentials {
     ExportCredentials exportCredentials;

    public ExportCredentialsDecorator(ExportCredentials exportCredentials) {
        this.exportCredentials = exportCredentials;
    }

    @Override
    public void export(List<Credentials> credentialsList) {
        exportCredentials.export(credentialsList);
    }
}

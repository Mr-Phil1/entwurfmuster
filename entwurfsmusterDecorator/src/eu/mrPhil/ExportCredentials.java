package eu.mrPhil;

import java.util.List;

public interface ExportCredentials {
    void export( List<Credentials> credentialsList);
}

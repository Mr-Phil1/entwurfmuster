package eu.mrPhil.chainOfResponsibility;

import eu.mrPhil.Credentials;

import java.util.List;

public class ExportBearbeiterJSON extends ExportBearbeiter {
    public ExportBearbeiterJSON(ExportBearbeiter naechsterExportBearbeiter) {
        super(naechsterExportBearbeiter);
    }

    @Override
    public void exportieren(String format, List<Credentials> credentialsList) {
        if (format.equals("JSON")) {
            String exportString = "[\n";
            for (int i = 0; i < credentialsList.size(); i++) {
                Credentials credentials = credentialsList.get(i);
                exportString += "{\"host\":" + "\"" + credentials.getHost() + "\"" + "," + "\"pwd\":" + "\"" + credentials.getUser() + "\"" + "," + "\"user\":" + "\"" + credentials.getPwd() + "\"" + "}";
                if (i < credentialsList.size() - 1) {
                    exportString += ",\n";
                }
            }
            exportString += "\n]";
            System.out.println(exportString);
        } else {
            super.exportieren(format, credentialsList);
        }
    }
}

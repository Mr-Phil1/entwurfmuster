package eu.mrPhil;

import eu.mrPhil.chainOfResponsibility.ExportBearbeiter;
import eu.mrPhil.chainOfResponsibility.ExportBearbeiterCSV;
import eu.mrPhil.chainOfResponsibility.ExportBearbeiterJSON;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // List mit der ausprobiert wird
        List<Credentials> credentials = new ArrayList<>();
        credentials.add(new Credentials("www.gmx.at", "123456789", "a.iller"));
        credentials.add(new Credentials("www.hotmail.com", "bhvghlvljh", "corban.nerum"));
        credentials.add(new Credentials("www.xyz.net", "5485f8e4sd", "bobba.fett"));

        // Chain of Responsibility

        ExportBearbeiter kette = new ExportBearbeiterCSV(new ExportBearbeiterJSON(null));
        kette.exportieren("CSV", credentials);
        System.out.println();
        kette.exportieren("JSON", credentials);
        System.out.println();
        kette.exportieren("Ddww", credentials);

    }
}
